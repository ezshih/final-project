#include "spaceship.h"
#include <QGraphicsScene>
#include <QKeyEvent>
#include "laser.h"
#include "asteroid.h"
#include "buffasteroid.h"
#include "bigasteroid.h"
#include "score.h"
#include "game.h"
extern Game* game;

spaceship::spaceship()
{
    strength = 1;
}

void spaceship::keyPressEvent(QKeyEvent *event)
{
//up down left right
    if(event->key() == Qt::Key_Left)
    {
        if (pos().x()>0)
            setPos(x()-10,y());
    }
    else if (event->key() == Qt::Key_Right)
    {
        if (pos().x()+100<800)
            setPos(x()+10,y());
    }
    else if (event->key() == Qt::Key_Up)
        setPos(x(),y()-10);
    else if (event->key() == Qt::Key_Down)
        setPos(x(),y()+10);
    //launches lasers according to spaceship strength
    else if (event->key() == Qt::Key_Space){
        if (strength == 1)
        {
            laser * Laser = new laser();
            scene()->addItem(Laser);
            Laser->setPos(x(),y());
        }
        if (strength == 2)
        {
            laser * Laser = new laser();
            laser * Laser2 = new laser();
            scene()->addItem(Laser);
            scene()->addItem(Laser2);
            Laser->setPos(x()-20,y());
            Laser2->setPos(x()+20,y());
        }
        if (strength == 3)
        {
            laser * Laser = new laser();
            laser * Laser2 = new laser();
            laser * Laser3 = new laser();
            scene()->addItem(Laser);
            scene()->addItem(Laser2);
            scene()->addItem(Laser3);
            Laser->setPos(x(),y());
            Laser2->setPos(x()-30,y());
            Laser3->setPos(x()+30,y());
        }

    }
}
//spawns asteroids and stuff
void spaceship::spawn()
{
    asteroid * enemy  = new asteroid();
    scene()->addItem(enemy);
}

void spaceship::spawnbuff()
{
    asteroid * enemy  = new buffasteroid();
    enemy->setBrush(Qt::green);
    scene()->addItem(enemy);
}
void spaceship::spawnbig()
{
    asteroid * enemy  = new bigasteroid();
    enemy->setBrush(Qt::black);
    scene()->addItem(enemy);
    game->score->increaseScore();

}
void spaceship::increase_strength()
{
    if (strength < 3)
        strength++;
}


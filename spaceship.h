#ifndef SPACESHIP_H
#define SPACESHIP_H

#include <QGraphicsRectItem>
#include <QObject>
//player
class spaceship: public QObject,public QGraphicsRectItem{
    Q_OBJECT
public:
    spaceship();
    void keyPressEvent(QKeyEvent * event);
    void increase_strength();

public slots:
    void spawn();
    void spawnbuff();
    void spawnbig();
private:
    int strength;

};

#endif // SPACESHIP_H

#include "laser.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include "asteroid.h"
#include "Game.h"
#include "buffasteroid.h"
#include "bigasteroid.h"
extern Game* game;

laser::laser()
{

    setRect(45,50,10,50);
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));//launch laser on creation
    timer->start(50);
}

void laser::move()
{
    //blows up enemies
    QList<QGraphicsItem *> asteroids_hit = collidingItems();
    for(int i = 0,n = asteroids_hit.size();i<n;i++){
        if (typeid(*(asteroids_hit[i])) == typeid(asteroid)){
                scene()->removeItem(asteroids_hit[i]);
                scene()->removeItem(this);
//add score
                game->score->increaseScore();
//delete asteroids and bullet

                delete asteroids_hit[i];
                delete this;
                return;
        }
        if (typeid(*(asteroids_hit[i])) == typeid(buffasteroid)){
            scene()->removeItem(asteroids_hit[i]);
            scene()->removeItem(this);
            game->buff();
//add score
            game->score->increaseScore();
//delete asteroids and bullet

            delete asteroids_hit[i];
            delete this;
            return;

    }
    }
    //deletes laser after reaches top of screen
    setPos(x(),y()-10);
    if (pos().y() + rect().height()<0)
    {
        scene()->removeItem(this);
        delete this;
    }
}

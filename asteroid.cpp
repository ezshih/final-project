#include "asteroid.h"
#include "spaceship.h"
#include <QTimer>
#include <QGraphicsScene>
#include <Qlist>
#include <stdlib.h>
#include <QDebug>
#include "game.h"
#include "score.h"
extern Game* game;

asteroid::asteroid(): QObject(),QGraphicsRectItem()
{
    //creates asteroids of random size and at random location
    int random = rand()%700;
    randomx = rand()%10 - 5;
    randomy = rand()%20;
    randomsize = rand()%50+50;

    setPos(random,0);
    setRect(0,0,randomsize,randomsize);
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));//
    timer->start(50);
}

void asteroid::move()
{
    //moves asteroid at random speed in random direction
    setPos(x()+randomx,y()+randomy);
    //destroys spaceship if hit
    QList<QGraphicsItem *> asteroids_hit = collidingItems();
    //kills spaceship
    for(int i = 0,n = asteroids_hit.size();i<n;i++){
        if (typeid(*(asteroids_hit[i])) == typeid(spaceship)){
                scene()->removeItem(asteroids_hit[i]);
                scene()->removeItem(this);
                game->score->gameover();
                game->dead = true;

//delete asteroids and bullet

                delete asteroids_hit[i];
                delete this;
                return;
        }
   }

//removes asteroids after passes bottom of screen
    if (pos().y() >600)
    {
        scene()->removeItem(this);
        delete this;
    }

}

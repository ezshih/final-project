#ifndef ASTEROID_H
#define ASTEROID_H
#include <QObject>
#include <QGraphicsRectItem>
//main enemy of game
class asteroid: public QObject,public QGraphicsRectItem{
    Q_OBJECT
public:
    asteroid();
public slots:
    void move();
private:
    int randomx;
    int randomy;
    int randomsize;
};

#endif // ASTEROID_H

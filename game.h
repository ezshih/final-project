#ifndef GAME_H
#define GAME_H
#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include "spaceship.h"
#include "score.h"
class Game: public QGraphicsView{
public:
    Game(QWidget * parent=0);
    QGraphicsScene *scene;
    spaceship * rect;
    Score * score;
    void buff();//buffs spaceships
    bool dead;//check if spaceship is dead
public slots:
    void replay();
};

#endif // GAME_H

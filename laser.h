#ifndef LASER_H
#define LASER_H
#include <QObject>
#include <QGraphicsRectItem>
//lasers kill asteroids
class laser: public QObject,public QGraphicsRectItem{
    Q_OBJECT
public:
    laser();
public slots:
    void move();
};

#endif // LASER_H

#ifndef SCORE_H
#define SCORE_H
#include <QGraphicsTextItem>
//score of game
class Score: public QGraphicsTextItem{
public:
    Score(QGraphicsItem *parent = 0);
    void increaseScore();
    int currentScore();
    void gameover();
private:
    int score;
};

#endif // SCORE_H

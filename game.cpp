#include "Game.h"
#include <Qtimer>
#include <QGraphicsTextItem>
#include "asteroid.h"
#include <QKeyEvent>
#include "laser.h"
#include <QDebug>

Game::Game(QWidget *parent)
{
    //initialize game

    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,800,600);
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(800,600);
    //create spaceship
    rect = new spaceship();
    rect->setRect(0,0,100,100);
    rect->setFlag(QGraphicsItem::ItemIsFocusable);
    rect->setFocus();
    rect->setBrush(Qt::red);
    scene->addItem(rect);
    //score
    score = new Score();
    scene->addItem(score);


    //starting player position
    rect->setPos(width()/2,height() - rect->rect().height());
    //spawn asteroids
    QTimer * timer = new QTimer();
    QObject::connect(timer,SIGNAL(timeout()),rect,SLOT(spawn()));
    timer->start(1000);
    //spawn buff asteroids
    QTimer * timerbuff = new QTimer();
    QObject::connect(timerbuff,SIGNAL(timeout()),rect,SLOT(spawnbuff()));
    timerbuff->start(10000);
//spawns buff asteroids
    QTimer * timerbig = new QTimer();
    QObject::connect(timerbig,SIGNAL(timeout()),rect,SLOT(spawnbig()));
    timerbig->start(5000);



}
void Game::buff()
{
    rect->increase_strength();
}



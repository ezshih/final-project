#include "score.h"
Score::Score(QGraphicsItem *parent): QGraphicsTextItem(parent)
{
    score = 0;

    setPlainText(QString("Score: ") + QString::number(score));
}

void Score::increaseScore()
{
    score += 10;
    setPlainText(QString("Score: ") + QString::number(score));

}

int Score::currentScore()
{
    return score;
}
void Score::gameover()
{
    setPlainText(QString("Game Over!!! Your Final Score: ") + QString::number(score));
}
